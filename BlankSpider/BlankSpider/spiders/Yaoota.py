import scrapy
from cleaner import string_cleaner,price_cleaner,cleanhtml,table_shaker,string_cleaner_array

class YaootaSpider(scrapy.Spider):
    name = 'Yaoota'
    allowed_domains = ['yaoota.com']
    start_urls = ['https://yaoota.com/en-eg/stores/']

    def parse(self, response):
        yield {
        "stores":string_cleaner_array(response.css('td.storesTable__table__storeName a::text').getall()),
        }
# If you don't understand a field set it to false.